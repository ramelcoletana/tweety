def vowel?(char)
	%w(a e i o u).include?(char.downcase)
end

def first(word)
	word[0,1]
end

def pig_latin(word)
	if(vowel?(first(word)))
		return word + "ay"
	else
		new_word = word[1..-1] + first(word)
		pig_latin(new_word)
	end
end

def translate(string)
	string = string.split(" ")
	pig_latin = []
	string.each do |word|
		if(vowel?(first(word)))
			pig_latin.push(word + 'way')
		else	
			pig_latin.push(pig_latin(word))
		end
	end
	return pig_latin.join(" ")
end
print "Enter String: "
string = gets.chomp
puts translate(string)



#puts "#{pig_latin.join(" ")}"
#string = "Love"
# def reverse(string)
# 	string = string.split("")
# 	temp = []
# 	s = string.size - 1
# 	string.each_with_index { |x,y| temp[s-y] = x}
# 	return temp.join("")
# end
# print "Enter string: "
# string = gets.chomp
# puts "The string is #{(string == reverse(string)) ? 'palindrome' : 'not palindrome'}."
