class UsersController < ApplicationController
	before_action :set_user, only: [:show, :edit, :update, :destroy, :correct_user]
	before_action :signed_in_user, only: [:index, :edit, :update, :destroy]
	before_action :correct_user, only: [:edit, :udpate]

	def index
		@users = User.all
	end

	def show
	end

	def new
		@user = User.new
	end

	def create
		@user = User.new(user_params)
		if @user.save
			flash[:success] = 'User account successfully created.'
			sign_in @user
			redirect_to @user
		else
			render 'new'
		end
	end

	def edit
	end

	def update
		if @user.update_attributes(user_params)
			flash[:success] = 'User account successfully updated.'
			redirect_to @user
		else
			render 'edit'
		end
	end

	def destroy
		User.find(params[:id]).destroy
		flash[:success] = "User destroyed!"
		redirect_to users_path
	end

	def followers
		@title = 'Followers'
		@user = User.find(params[:id])
		@users = @user.followers
		render 'show_follow'
	end

	def following
		@title = 'Following'
		@user = User.find(params[:id])
		@users = @user.followed_users
		render 'show_follow'
	end

	private
		def set_user
			@user = User.find(params[:id])
		end

		def user_params
			params.require(:user).permit(:name, :email, :password, :password_confirmation)
		end

		# Before filters
		def correct_user
			redirect_to(root_path) unless current_user?@user
		end
end